include .env

DOCKER_COMPOSE = docker-compose
NODE 		   = ${DOCKER_COMPOSE} run --rm node

install: node_modules

start:
	${DOCKER_COMPOSE} up -d

stop:
	${DOCKER_COMPOSE} down

restart: stop start

open:
	xdg-open http://${DOMAIN_NAME}:${APP_PORT}

build:
	${NODE} npm run build

kill:
	$(DOCKER_COMPOSE) kill
	$(DOCKER_COMPOSE) down --volumes --remove-orphans

clean: kill
	rm -rf node_modules

reset: clean install

node:
	$(DOCKER_COMPOSE) run --rm node bash

.PHONY: install start stop restart open build kill clean reset node

node_modules: package-lock.json
	${NODE} npm ci
	@touch -c node_modules

package-lock.json: package.json
	$(NODE) npm install

.PHONY: node_modules
